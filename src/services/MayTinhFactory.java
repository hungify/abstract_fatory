package services;

public class MayTinhFactory {


	public static MayTinhAbstractFactory getFactory() {
		// Logic
		int min = 1;
		int max = 10;

		int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
		if (random_int > 5) {
			return new MayTinhSamSungFactory();
		} else {
			return new MayTinhAppleFactory();
		}
	}
}

